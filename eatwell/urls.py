"""myorder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from eatwell import views

urlpatterns = [
    path('index/', views.index, name='index'),

    path('menu_add/', views.menu_add, name='menu_add'),
    path('menus/', views.menus, name='menus'),
    path('pay/', views.pay, name='pay'),
    path('register/', views.register, name='register'),
    path('showuser/', views.showuser, name='showuser'),
    path('loginx/', views.loginx, name='loginx'),
    path('restaurants/', views.restaurants, name='restaurants'),
    path('orders/', views.orders, name='orders'),

    # 添加餐馆
    path('restaurant_add/', views.restaurant_add, name='restaurant_add'),
    path('logoutx/', views.logoutx, name='logoutx'),

]
