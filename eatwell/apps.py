from django.apps import AppConfig


class EatwellConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eatwell'
